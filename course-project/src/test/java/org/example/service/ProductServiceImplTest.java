package org.example.service;

import org.example.entity.ProductEntity;
import org.example.repository.ProductRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProductServiceImplTest {
    private ProductEntity productEntity;
    private ArrayList<ProductEntity> products;
    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    @Mock
    private ProductRepositoryImpl productRepository;
    @InjectMocks
    private ProductServiceImpl productService;

    @BeforeEach
    public void setUp() {
        initMocks(this);
        productEntity = new ProductEntity();
        productEntity.setId(1L);
        productEntity.setName("test");
        productEntity.setDescription("test");
        productEntity.setCategory("TEST");
        productEntity.setPrice(10.0);
        productEntity.setQuantity(5);
        products = new ArrayList<>();
        products.add(productEntity);
        System.setOut(new PrintStream(output));
    }
    @Test
    public void getAllProductsTest() {
        when(productRepository.getAllProducts()).thenReturn(products);
        productService.viewAllProducts();
        assertEquals("ProductEntity{Id=1, name='test', description='test', category='TEST', price=10.0, quantity=5}\r\n", output.toString());
    }
    @Test
    public void searchByNameTest() {
        when(productRepository.getByName("test")).thenReturn(products);
        productService.searchByName("test");
        assertEquals("ProductEntity{Id=1, name='test', description='test', category='TEST', price=10.0, quantity=5}\r\n", output.toString());
    }
    @Test
    public void searchByNameIfTest() {
        when(productRepository.getByName("test")).thenReturn(new ArrayList<>());
        productService.searchByName("test");
        assertEquals("No products found for this name\r\n", output.toString());
    }
    @Test
    public void searchByCategoryTest() {
        when(productRepository.getByCategory("TEST")).thenReturn(products);
        productService.searchByCategory("TEST");
        assertEquals("ProductEntity{Id=1, name='test', description='test', category='TEST', price=10.0, quantity=5}\r\n", output.toString());
    }
    @Test
    public void searchByCategoryIfTest() {
        when(productRepository.getByCategory("TEST")).thenReturn(new ArrayList<>());
        productService.searchByCategory("TEST");
        assertEquals("No products found for this category \r\n", output.toString());
    }
    @Test
    public void searchByPriceTest() {
        when(productRepository.getByPrice(10.0)).thenReturn(products);
        productService.searchByPrice(10.0);
        assertEquals("ProductEntity{Id=1, name='test', description='test', category='TEST', price=10.0, quantity=5}\r\n", output.toString());
    }
    @Test
    public void searchByPriceIfTest() {
        when(productRepository.getByPrice(10.0)).thenReturn(new ArrayList<>());
        productService.searchByPrice(10.0);
        assertEquals("No products found for this price \r\n", output.toString());
    }

}