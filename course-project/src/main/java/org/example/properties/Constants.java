package org.example.properties;

import java.util.Scanner;

public interface Constants {
    Scanner strScanner = new Scanner(System.in);
    Scanner intScanner = new Scanner(System.in);
    String APP_NAME = "Cosmetics warehouse";
    String VERSION = "1.0.0";
    String CREATION_DATE="May 17, 2023";
    String DEVELOPMENT_EMAIL="Bilol_Pardabaev@student.itpu.uz";
    String fileUrl="src/main/resources/products.csv";
    String aboutProgram = "";
}
