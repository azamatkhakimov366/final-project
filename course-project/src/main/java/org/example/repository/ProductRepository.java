package org.example.repository;

import org.example.entity.ProductEntity;

import java.util.ArrayList;

public interface ProductRepository {
    ArrayList<ProductEntity> getAllProducts();
    ArrayList<ProductEntity> getByName(String name);
    ArrayList<ProductEntity> getByCategory(String category);
    ArrayList<ProductEntity> getByPrice(Double price);
}
