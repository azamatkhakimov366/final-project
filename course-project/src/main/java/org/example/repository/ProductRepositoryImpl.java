package org.example.repository;

import org.example.entity.ProductEntity;
import org.example.properties.Constants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class ProductRepositoryImpl implements ProductRepository{
    @Override
    public ArrayList<ProductEntity> getAllProducts() {
        ArrayList<ProductEntity> products = new ArrayList<ProductEntity>();
        try (BufferedReader bufferedReader= new BufferedReader(new FileReader(Constants.fileUrl))){
            String line;
            while ((line = bufferedReader.readLine())!= null){
                String[] split = line.split(",");
                ProductEntity productEntity = new ProductEntity();
                productEntity.setId(Long.parseLong(split[0]));
                productEntity.setName(split[1]);
                productEntity.setDescription(split[2]);
                productEntity.setCategory(split[3]);
                productEntity.setPrice(Double.parseDouble(split[4]));
                productEntity.setQuantity(Integer.parseInt(split[5]));
                products.add(productEntity);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public ArrayList<ProductEntity> getByName(String name) {
        ArrayList<ProductEntity> products = new ArrayList<ProductEntity>();
        for (ProductEntity productEntity: getAllProducts()) {
            if (productEntity.getName().toUpperCase().contains(name.toUpperCase())) {
                products.add(productEntity);
            }
        }
        return products;
    }

    @Override
    public ArrayList<ProductEntity> getByCategory(String category) {
        ArrayList<ProductEntity> products = new ArrayList<ProductEntity>();
        for (ProductEntity productEntity: getAllProducts()) {
            if (Objects.equals(productEntity.getCategory().toUpperCase(), category.toUpperCase())) {
                products.add(productEntity);
            }
        }return products;
    }

    @Override
    public ArrayList<ProductEntity> getByPrice(Double price) {
        ArrayList<ProductEntity> products = new ArrayList<ProductEntity>();
        for (ProductEntity productEntity: getAllProducts()) {
            if (productEntity.getPrice()<=price) {
                products.add(productEntity);
            }
        }return products;
    }
}
