package org.example;

import org.example.controller.Controller;
import org.example.controller.ControllerImpl;

public class Main {
    public static void main(String[] args){
        Controller controller = new ControllerImpl();
        controller.mainController();
    }
}