package org.example.service;

public interface ProductService {
    void viewAllProducts();
    void searchByName(String name);
    void searchByCategory(String category);
    void searchByPrice(Double price);
}
