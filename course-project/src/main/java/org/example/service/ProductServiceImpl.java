package org.example.service;

import org.example.entity.ProductEntity;
import org.example.repository.ProductRepository;
import org.example.repository.ProductRepositoryImpl;

import java.util.ArrayList;

public class ProductServiceImpl implements ProductService{
    ProductRepository productRepository = new ProductRepositoryImpl();
    @Override
    public void viewAllProducts() {
        productRepository.getAllProducts().forEach(System.out::println);
    }

    @Override
    public void searchByName(String name) {
        ArrayList<ProductEntity> byName = productRepository.getByName(name);
        if (byName.isEmpty()) {
            System.out.println("No products found for this name");
        }
        byName.forEach(System.out::println);
    }

    @Override
    public void searchByCategory(String category) {
        ArrayList<ProductEntity> byCategory = productRepository.getByCategory(category);
        if (byCategory.isEmpty()) {
            System.out.println("No products found for this category ");
        }
        byCategory.forEach(System.out::println);

    }

    @Override
    public void searchByPrice(Double price) {
        ArrayList<ProductEntity> byPrice = productRepository.getByPrice(price);
        if (byPrice.isEmpty()) {
            System.out.println("No products found for this price ");
        }
        byPrice.forEach(System.out::println);
    }
}
