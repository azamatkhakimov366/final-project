package org.example.controller;

import org.example.properties.Constants;
import org.example.service.ProductService;
import org.example.service.ProductServiceImpl;

import java.security.Provider;

public class ProductControllerImpl implements ProductController{
    @Override
    public void productController() {
        ProductService productService = new ProductServiceImpl();
        String productMenu = """
                           Product menu
                0.Exit main menu
                1.View all products;
                2.Search by name;
                3.Search by category;
                4.Search by price
                """;
        while (true){
            System.out.println(productMenu);
            System.out.println("Select any menu");
            int command = Constants.intScanner.nextInt();
            switch (command){
                case 0 ->{return;}
                case 1 ->productService.viewAllProducts();
                case 2 ->{
                    System.out.println("Enter product name");
                    String name = Constants.strScanner.nextLine();
                    productService.searchByName(name);
                }
                case 3 ->{
                    System.out.println("Enter product category");
                    String category = Constants.strScanner.nextLine();
                    productService.searchByCategory(category);
                }
                case 4 ->{
                    System.out.println("Enter product price");
                    Double price = Constants.intScanner.nextDouble();
                    productService.searchByPrice(price);
                }
            }
        }
    }
}
