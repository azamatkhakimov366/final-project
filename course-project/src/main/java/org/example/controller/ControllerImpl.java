package org.example.controller;

import org.example.properties.Constants;

public class ControllerImpl implements Controller{
    @Override
    public void mainController() {
        ProductController productController=new ProductControllerImpl();
        System.out.println(Constants.APP_NAME);
        System.out.println(Constants.VERSION);
        System.out.println(Constants.CREATION_DATE);
        System.out.println(Constants.DEVELOPMENT_EMAIL + "\n\n");
        String menu= """
                0.Exit
                1.Product menu
                2.About the program
                """;
        while (true){
            System.out.println(menu);
            System.out.println("Enter menu number: ");
            int menuNumber = Constants.intScanner.nextInt();
            switch (menuNumber){
                case 0 -> {
                    System.out.println("Exiting program...");
                    return;
                }
                case 1->productController.productController();
                case 2->{
                    System.out.println(Constants.aboutProgram);
                }
            }
        }
    }
}
